VERSION := $(shell git rev-parse --short HEAD)
BUILDTIME := $(shell date -u '+%Y-%m-%dT%H:%M:%SZ')
BUILDTIMEFILENAME := $(shell date -u '+%Y%m%d-%H%M%SZ')
BUILDTIMETAG := $(shell date -u '+%Y%m%d%H%M%S')
BUILDUSER := $(shell whoami)
BUILDHOST := $(shell hostname -s)
BUILDARCH := $(shell uname -m)

FN := formless
IMAGENAME := sneak/$(FN)

UNAME_S := $(shell uname -s)

GOLDFLAGS += -X main.Version=$(VERSION)
GOLDFLAGS += -X main.Buildarch=$(BUILDARCH)

# osx can't statically link apparently?!
ifeq ($(UNAME_S),Darwin)
	GOFLAGS := -ldflags "$(GOLDFLAGS)"
endif

ifneq ($(UNAME_S),Darwin)
	GOFLAGS = -ldflags "-linkmode external -extldflags -static $(GOLDFLAGS)"
endif

default: fmt

fmt:
	go fmt ./...
	goimports -l -w .

debug: build
	GOTRACEBACK=all FORMLESS_DEBUG=1 ./$(FN) 2>&1 | tee -a debug.log

run: build
	./$(FN)

clean:
	-rm ./$(FN)

build: ./$(FN)

.lintsetup:
	go get -v -u golang.org/x/lint/golint
	go get -u github.com/GeertJohan/fgt
	touch .lintsetup

lint: fmt .lintsetup
	fgt golint ./...

go-get:
	cd cmd/$(FN) && go get -v

./$(FN): */*.go cmd/*/*.go go-get
	cd cmd/$(FN) && go build -o ../../$(FN) $(GOFLAGS) .

test: lint build-docker-image

is_uncommitted:
	git diff --exit-code >/dev/null 2>&1

docker:
	docker build -t $(IMAGENAME) .

build-docker-image-dist: is_uncommitted clean
	docker build -t $(IMAGENAME):$(VERSION) -t $(IMAGENAME):latest -t $(IMAGENAME):$(BUILDTIMETAG) .

dist: lint docker
	-mkdir -p ./output
	docker run --rm --entrypoint cat $(IMAGENAME) /bin/$(FN) > output/$(FN)
	docker save $(IMAGENAME) | bzip2 > output/$(BUILDTIMEFILENAME).$(FN).tbz2

hub: upload-docker-image

upload-docker-image: docker
	docker push $(IMAGENAME):$(VERSION)
	docker push $(IMAGENAME):$(BUILDTIMETAG)
	docker push $(IMAGENAME):latest

vet:
	go vet ./...
	bash -c 'test -z "$$(gofmt -l .)"'

.PHONY: build fmt test is_uncommitted docker dist hub upload-docker-image clean run rundebug default build-docker-image-dist
