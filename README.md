# formless

Forum software.  Server rendered, HTML!

## Explicit Design Goals

* Server-rendered HTML
* 100% functionality without any clientside JS
* Fast
* Simple

# Build Status

[![Build Status](https://drone.datavi.be/api/badges/sneak/formless/status.svg)](https://drone.datavi.be/sneak/formless)

# Author

https://sneak.berlin

sneak@sneak.berlin

# License

WTFPL
