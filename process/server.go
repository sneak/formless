package process

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	gl "github.com/labstack/gommon/log"
	ep2 "github.com/mayowa/echo-pongo2"
	"github.com/ziflex/lecho"

	//"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"
)

// Server is the HTTP webserver object
type Server struct {
	formless   *Formless
	port       uint
	e          *echo.Echo
	router     *gin.Engine
	httpserver *http.Server
	debug      bool
	db         *gorm.DB
}

func (a *Server) SetFormless(f *Formless) {
	a.formless = f
}

func (a *Server) Serve() {

	if a.formless == nil {
		panic("must have app from which to serve")
	}

	if os.Getenv("DEBUG") != "" {
		a.debug = true
	}

	a.port = 8080

	if os.Getenv("PORT") != "" {
		s, err := strconv.ParseUint(os.Getenv("PORT"), 10, 64)
		if err != nil {
			a.port = uint(s)
		}
	}

	a.initRouter()
	a.initServer()
	a.e.Logger.Fatal(a.e.StartServer(a.httpserver))
}

func (s *Server) initRouter() {

	// Echo instance
	s.e = echo.New()

	s.e.HideBanner = true

	lev := gl.INFO
	if os.Getenv("DEBUG") != "" {
		lev = gl.DEBUG
	}

	logger := lecho.New(
		os.Stdout,
		lecho.WithLevel(lev),
		lecho.WithTimestamp(),
		lecho.WithCaller(),
	)
	s.e.Logger = logger
	s.e.Use(middleware.RequestID())

	// Middleware
	s.e.Use(middleware.Logger())
	s.e.Use(middleware.Recover())

	r, err := ep2.NewRenderer("view")
	if err != nil {
		s.e.Logger.Fatal(err)
	}
	s.e.Renderer = r

	// Routes
	s.e.GET("/", s.indexHandler)
	s.e.GET("/.well-known/healthcheck.json", s.healthCheckHandler)
	// FIXME add prometheus metrics output
	//a.e.GET("/about", s.aboutHandler)

}

func (s *Server) initServer() {
	log.Info().Uint("port", s.port).Msg("starting webserver")

	s.httpserver = &http.Server{
		Addr:           fmt.Sprintf(":%d", s.port),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
}
