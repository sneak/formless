package process

import (
	"net/http"
	"time"

	"github.com/flosch/pongo2"
	"github.com/labstack/echo"
)

type hash map[string]interface{}

func (a *Server) notFoundHandler(c echo.Context) error {
	return c.String(http.StatusNotFound, "404 not found")
}

func (a *Server) indexHandler(c echo.Context) error {
	tc := pongo2.Context{}
	return c.Render(http.StatusOK, "index.html", tc)
}

func (a *Server) healthCheckHandler(c echo.Context) error {
	resp := &hash{
		"status": "ok",
		"now":    time.Now().UTC().Format(time.RFC3339),
		"uptime": a.formless.uptime().String(),
	}
	return c.JSONPretty(http.StatusOK, resp, "    ")

}
