package database

import (
	"time"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type User struct {
	gorm.Model
	UUID     uuid.UUID `gorm:"type:uuid;primary_key;"`
	Username string    `gorm:"unique_index"`
	Created  time.Time
}

type Post struct {
	gorm.Model
	UUID uuid.UUID `gorm:"type:uuid;primary_key;"`
	Body string
}

// NB that when you add a model below you must add it to this list!
func (m *Manager) doMigrations() {
	log.Info().Msg("doing database migrations if required")
	m.db.AutoMigrate(&User{})
	m.db.AutoMigrate(&Post{})
}
