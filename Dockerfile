## build image:
ARG GO_VERSION=1.15
FROM golang:${GO_VERSION}-alpine AS builder

RUN mkdir /user && \
    echo 'nobody:x:65534:65534:nobody:/:' > /user/passwd && \
    echo 'nobody:x:65534:' > /user/group
RUN apk add --no-cache ca-certificates git bzip2 make gcc libc-dev

RUN mkdir -p /go/src/git.eeqj.de/sneak/formless
WORKDIR /go/src/git.eeqj.de/sneak/formless

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY ./ ./
RUN make build
RUN tar -c /go | bzip2 > /go.tbz2

## output image:
FROM scratch as final
COPY --from=builder /user/group /user/passwd /etc/
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/git.eeqj.de/sneak/formless/formless /app/formless
COPY --from=builder /go.tbz2 /go.tbz2

WORKDIR /app

ENV PORT 8080
ENV DBURL none

EXPOSE 8080

USER nobody:nobody

ENTRYPOINT ["./formless"]
