module git.eeqj.de/sneak/formless

go 1.15

require (
	git.eeqj.de/sneak/goutil v0.0.0-20200330225716-cdefc88d8f46
	github.com/flosch/pongo2 v0.0.0-20200913210552-0d938eb266f3
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.2
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0
	github.com/mattn/go-isatty v0.0.12
	github.com/mayowa/echo-pongo2 v0.0.0-20170410154925-661ce95e1767
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/rs/zerolog v1.20.0
	github.com/spf13/viper v1.7.1
	github.com/ziflex/lecho v1.2.0
)
